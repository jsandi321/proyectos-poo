import java.util.ArrayList;
import java.util.Scanner;

/**********************************************************************
 Instituto Tecnológico de Costa Rica
 Estructuras de Datos IC-2001
 II Semestre 2018
 Profesora: Samanta Ramijan Carmiol
 Estudiante: Emmanuel Murillo Sánchez || Carné: 201820130
 Estudiante: Jeison Sandi Mena || Carné: 2016093589

 Ejemplos Prácticos: Aplicacion Completa
 **********************************************************************/
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el nombre del supermercado: ");
        String superName = sc.nextLine();
        Supermercado super1 = new Supermercado(superName);
        AdministradorArchivo aA = new AdministradorArchivo(super1);
        aA.leerArchivo();
        
        System.out.println("Bienvenido a "+super1.getNombre());
        boolean salida = false;
        while (salida != true){
            System.out.println("Por favor elija una de las siguientes opciones :");
            System.out.println("1. Crear Menbresias");
            System.out.println("2. Registrar Clientes");
            System.out.println("3. Ver Clientes");
            System.out.println("4. Ver Membresias");
            System.out.println("5. Mostrar Inventario existente");
            int opcion = sc.nextInt();
            if(opcion == 1){
                System.out.println("Ingrese el tipo de membresia");
                String tipoMembresia = sc.next();
                System.out.println("Ingrese el porcentaje de descuento de la membresia: ");
                float porcentaje = sc.nextFloat();
                super1.agregarMembresia(tipoMembresia, porcentaje);
                System.out.println("Se ha registrado la membresia");
            }
            else if(opcion == 2) {
                System.out.println("Vamos a registrar algunos clientes, por favor ingrese la cantidad que desea registrar: ");
                int numClientes = sc.nextInt();
                for (int i = 0; i < numClientes; i++) {
                    System.out.println("Que tipo de cliente desea registrar?");
                    System.out.println("1. Con membresia");
                    System.out.println("2. Sin membresia");
                    int tipoCliente = sc.nextInt();
                    if (tipoCliente == 1) {
                        System.out.println("Ingrese el nombre del cliente: ");
                        String nombreCliente = sc.next();

                    } else if (tipoCliente == 2) {
                        System.out.println("Ingrese el nombre del cliente: ");
                        String nombreCliente = sc.next();
                        super1.agregarCliente(nombreCliente);
                    }
                }
            }
            else if(opcion == 3){
                super1.imprimirClientes();
            }
            else if(opcion == 4){
                super1.imprimirMembresias();
            }
            else if(opcion == 5){
                super1.imprimirInventario();
            
            }
        }
    }
}
