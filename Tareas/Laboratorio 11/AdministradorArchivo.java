 /**********************************************************************
 Instituto Tecnológico de Costa Rica
 Estructuras de Datos IC-2001
 II Semestre 2018
 Profesora: Samanta Ramijan Carmiol
 Estudiante: Emmanuel Murillo Sánchez || Carné: 201820130
 Estudiante: Jeison Sandi Mena || Carné: 2016093589
 
 Ejemplos Prácticos: Aplicacion Completa
 **********************************************************************/

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import java.io.FileNotFoundException;
import java.io.IOException;

 
public class AdministradorArchivo {
 
// Atributos locales
    
    private Supermercado supermercado;
    private String Archivo = "productos.xls";
   
//Datos contenidos en el archivo
    
    private String nombre;
    private double precioSinImp;
    private double porcentajeImp;
    private String tipoDeVenta;

    
//Constructor

    public AdministradorArchivo(Supermercado supermercado) { 
        this.supermercado = supermercado; 
    }
    
//Lector de archivo

    public void leerArchivo(){
        try {
            FileInputStream file = new FileInputStream(new File(Archivo));
            
            // leer archivo excel                      
            XSSFWorkbook workbook = new XSSFWorkbook(file); 
            
            //obtener la hoja que se va leer
            XSSFSheet sheet = workbook.getSheetAt(0);
            
            //obtener todas las filas de la hoja excel
            Iterator<Row> iterator = sheet.iterator();

           
            //iterator.next();
            
            Row row;
            
            // se recorre cada fila hasta el final
            while (rowIterator.hasNext()) {

                row = rowIterator.next();
                
                //se obtiene las celdas por fila
                Iterator<Cell> cellIterator = row.celliterator();
                
                Cell cell;
                
		//se recorre cada celda
                while (cellIterator.hasNext()) {
                    
                    // se obtiene la celda en específico
                    cell = cellIterator.next();
                    
                    //se obtiene la celda por columna 
                    int columnIndex = columnCell.getColumnIndex();
                    
                    
                    //Dependiendo la celda cosultada se guarda la información correspondiente
                    switch (columnIndex) {
                        
                        case 0:
                            
                            nombre = columnCell.getStringCellValue();
                            
                        break;
                            
                        case 1:
                            
                            precioSinImp = (double)columnCell.getNumericCellValue();
                            
                        break;
                            
                        case 2:
                            
                            porcentajeImp = (double)columnCell.getNumericCellValue();
                            
                        break;
                        
                        case 3:
                            
                            tipoDeVenta = columnCell.getStringCellValue();
                          
                            if(tipoDeVenta == "UNIDAD"){
                                
                                supermercado.agregarProductoInventarioVentaUnitaria(nombre,precioSinImpuesto,porcentajeImpuesto);
                            
                            }else{
                                
                                supermercado.agregarProductoInventarioVentaPorPeso(nombre,precioSinImpuesto,porcentajeImpuesto);
                            
                            }
                        break;
                    }
                }
            }
        }catch(FileNotFoundException e) {
            e.printStackTrace();
            
            
        }catch(IOException e) {
            e.printStackTrace();
        }
        
    }
    
}

