//Jeison Sandi Mena 2016093589
import java.util.Scanner;

public class ATM{
	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
    //Arreglo con los objetos Cuenta
		Cuenta[] arregloCuentas= new Cuenta[10];
		//Crracion de arreglo introduciondo los objetos.
		for(int i= 0; i < arregloCuentas.length; i++) {
			arregloCuentas[i] = new Cuenta(i, 100000,0);
		}
    //Condicion de parada para el menu
		boolean parada= false;
    //While para mantener el menu activo
		while(!parada) {
			System.out.println("Ingrese su id: ");
			int id= sc.nextInt();
			if(id>9 || id < 0){
				System.out.println("Ingresa un valor valido: ");
				id= sc.nextInt();
			}
			Cuenta cuentaUsuario= arregloCuentas[id];
      //Condicion de parada para Menu principal
			boolean salida= false;
      //Ciclo del menu Principal
			while(!salida) {
				System.out.println("Menu Principal"
						+ "1.Ver el balance general."
						+" 2.Retirar dinero."
						+ "3. Depositar dinero."
						+ "4.Salir");
				int opcion = sc.nextInt();
				if(opcion == 1){
					double balanceGeneral= cuentaUsuario.getBalance();
					System.out.println("El balance es de: "+balanceGeneral);
				}
				if(opcion == 2){
					System.out.println("Ingrese la cantidad para retirar: ");
					double retiro = sc.nextDouble();
					cuentaUsuario.retirarDinero(retiro);
					}
				if(opcion == 3) {
					System.out.println("Ingrese una cantidad para depositar: ");
					double deposito = sc.nextDouble();
					cuentaUsuario.depositarDinero(deposito);
					}
				if(opcion == 4) {
					salida= true;
					}
				else {
					
				}
			}
		}
	}
} 