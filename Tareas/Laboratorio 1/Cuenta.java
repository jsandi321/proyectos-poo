//Jeison Sandi Mena 2016093589
import java.util.Date;

public class Cuenta{
  private int id;
  private double balance;
  private double tasaInteresAnual;
  private Date fechaDeCreacionDate;

  public Cuenta(){
    id = id;
    balance= balance;
    tasaInteresAnual = tasaInteresAnual;
    fechaDeCreacionDate = fechaDeCreacionDate;
  }

  public Cuenta(int id, double balance, double tasaInteresAnual){
    this.id = id;
    this.balance = balance;
    this.tasaInteresAnual = tasaInteresAnual;
    fechaDeCreacionDate = fechaDeCreacionDate;
  }

  public int getId(){
    return id;
  }
  public void setId(int id){
    this.id = id;
  }
  public double getBalance(){
    return balance;
  }
  public void setBalance(double balance){
    this.balance= balance;
  }
  public double getTasaInteresAnual(){
    return tasaInteresAnual;
  }
  public void setTasaInteresAnual(){
    this.tasaInteresAnual = tasaInteresAnual; 
  }
  public Date getFechaDeCreacion(){
    return fechaDeCreacionDate;
  }

  public double obtenerTasaInteresMensual(){
    double tasaInteresMensual = (tasaInteresAnual/12);
    return tasaInteresMensual;
  }

  public double calcularInteresMensual(){
    double interesMensual = (balance * tasaInteresAnual);
    return interesMensual;
  }

  public double retirarDinero(double monto){
    balance -= monto;
    return balance;
  }
  public double depositarDinero(double monto){
    balance += monto;
    return balance;
  }
}