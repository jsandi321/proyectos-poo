//Jeison Sandi Mena 2016093589

class Main {
  public static void main(String[] args) {
    //Cracion de objeto
    Cuenta c1= new Cuenta(1122, 500000, 4.5);
    //Metodo de objeto c1 para depositar
    c1.depositarDinero(150000);
    //Metodo de objeto c1 para retirar dinero
    c1.retirarDinero(200000);
    System.out.println(" Balance: "
                      + c1.getBalance()
                      +" Interes: "
                      + c1.getTasaInteresAnual()
                      + "Fecha de creacion: "+c1.getFechaDeCreacion());
    //Creacion de nuevo objeto.
    Cuenta c2 = new Cuenta (9712, 100000, 3.5);
    System.out.println("Balance: "
                      + c2.getBalance()
                      +" Interes: "
                      +c2.getTasaInteresAnual()
                      +" Fecha de creacion: "
                      + c2.getFechaDeCreacion());
    
  }
}