
/**
 *Jeison Sandi Mena
 * 2016093589
 * @author jsandi321
 */
import java.util.ArrayList;

public class PilaTEC<Object> extends ArrayList<Object>{
    
    public boolean isEmpty(){
        return super.isEmpty();
    }
    
    public int getSize(){
        return super.size();
    }
    
    public Object peek(){
        return super.get(size()-1);
    }
    
    public Object pop(){
        Object o = super.get(size()-1);
        super.remove(size()-1);
        return o;
    }
    
    public void push(Object o){
        super.add(o);
    }
}

