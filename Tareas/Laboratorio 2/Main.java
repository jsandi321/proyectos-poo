import java.util.Scanner;

/**
 *Jeison Sandi Mena
 * 2016093589
 * @author jsandi321
 */

public class Main {    
    public static void main(String[] args) {
        // TODO code application logic here
        PilaTEC<Object> pila = new PilaTEC();
        Scanner sc = new Scanner(System.in);
        
        for(int i = 0; i < 5; i++ ){
            System.out.println("Ingrese un valor entero: ");
            Object num = sc.nextInt();
            pila.push(num);
        }
        for(int i = (pila.getSize()-1); i >= 0; i--){
            System.out.println("Valor :"+ pila.get(i));
        }
    }
}
